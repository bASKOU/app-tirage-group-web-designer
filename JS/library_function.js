/**
 * Generate HTML elements dynamically on index page
 * It takes elements from .CSV importation and array resulting from it
 */
//Flag for check if divs empty
let flag = false;
//Generate reserve if have
function generateReserve(table) {
    let len = table.length-1;
    let div = $("<div/>");
    let ol = $("<ol/>");
    let legend = $("<legend/>");
    div.css("width", "42%");
    legend.text("Réserve");
    ol.attr("data-draggable", "target");
    $(".reserve").append(div);
    div.append(legend, ol).hide().fadeIn(2000);
    for ( i = 0; i < table[len].length; i++) {
        let li = $("<li/>");
        let button = $("<button/>");
        let button1 = $("<button/>");
        button.attr("class", "moins").html("&#x2b9f");
        button1.attr("class", "plus").html("&#x2b9d");
        li.text(table[len][i]['Nom']+" "+table[len][i]['Prenom']);
        li.attr({"data-object": JSON.stringify(table[len][i]),"data-draggable":"item"});
        button1.appendTo(li);
        button.prependTo(li);
        ol.append(li);
    }
}
//generate li in ol depending on
function generateLiInOl(table) {
    for ( i = 0; i < table.length-1; i++) {
        let div = $("<div/>");
        let ol = $("<ol/>");
        let legend = $("<legend/>");
        div.attr("class", "groups");
        legend.text("Groupe"+(i+1));
        ol.attr({"class": "group"+i, "data-draggable": "target"});
        $(".append").append(div);
        div.append(legend).hide().fadeIn(500);
        div.append(ol).hide().fadeIn(500);
        for ( j = 0; j < table[i].length; j++){
            let li = $("<li/>");
            let button = $("<button/>");
            let button1 = $("<button/>");
            button.attr("class", "moins").html("&#x2b9f");
            button1.attr("class", "plus").html("&#x2b9d");
            li.text(table[i][j]["Nom"]+" "+table[i][j]["Prenom"]);
            li.attr({"data-object": JSON.stringify(table[i][j]), "data-draggable": "item"});
            button1.appendTo(li);
            button.prependTo(li);
            ol.append(li);
        }
    }
}
/**
 * Drag and drop functions
 */
function dragAndDrop() {
    //get the collection of draggable targets and add their draggable attribute
    for(var targets = document.querySelectorAll('[data-draggable="target"]'), len = targets.length, i = 0; i < len; i ++) {
        targets[i].setAttribute('aria-dropeffect', 'none');
    }
    //get the collection of draggable items and add their draggable attributes
    for(var items = document.querySelectorAll('[data-draggable="item"]'), len = items.length, i = 0; i < len; i ++) {
        items[i].setAttribute('draggable', 'true');
        items[i].setAttribute('aria-grabbed', 'false');
        items[i].setAttribute('tabindex', '0');
    }
    //dictionary for storing the selections data 
    //comprising an array of the currently selected items 
    //a reference to the selected items' owning container
    //and a refernce to the current drop target container
    var selections = 
    {
        items      : [],
        owner      : null,
        droptarget : null
    };
    //function for selecting an item
    function addSelection(item){
        //if the owner reference is still null, set it to this item's parent
        //so that further selection is only allowed within the same container
        if(!selections.owner) {selections.owner = item.parentNode;}
        //or if that's already happened then compare it with this item's parent
        //and if they're not the same container, return to prevent selection
        else if(selections.owner != item.parentNode) {
            return;
        }  
        //set this item's grabbed state
        item.setAttribute('aria-grabbed', 'true');
        //add it to the items array
        selections.items.push(item);
    }
    //function for unselecting an item
    function removeSelection(item) {
        //reset this item's grabbed state
        item.setAttribute('aria-grabbed', 'false');
        //then find and remove this item from the existing items array
        for(var len = selections.items.length, i = 0; i < len; i ++){
            if(selections.items[i] == item) {
                selections.items.splice(i, 1);
                break;
            }
        }
    }
    //function for resetting all selections
    function clearSelections() {
        //if we have any selected items
        if(selections.items.length) {
            //reset the owner reference
            selections.owner = null;
            //reset the grabbed state on every selected item
            for(var len = selections.items.length, i = 0; i < len; i ++) {
                selections.items[i].setAttribute('aria-grabbed', 'false');
            }
            //then reset the items array        
            selections.items = [];
        }
    }
    //shorctut function for testing whether a selection modifier is pressed
    function hasModifier(e) {
        return (e.ctrlKey || e.metaKey || e.shiftKey);
    }
    //function for applying dropeffect to the target containers
    function addDropeffects() {
        //apply aria-dropeffect and tabindex to all targets apart from the owner
        for(var len = targets.length, i = 0; i < len; i ++) {
            if(targets[i] != selections.owner && targets[i].getAttribute('aria-dropeffect') == 'none'){
                targets[i].setAttribute('aria-dropeffect', 'move');
                targets[i].setAttribute('tabindex', '0');
            }
        }
        //remove aria-grabbed and tabindex from all items inside those containers
        for(var len = items.length, i = 0; i < len; i ++){
            if(items[i].parentNode != selections.owner && items[i].getAttribute('aria-grabbed')){
                items[i].removeAttribute('aria-grabbed');
                items[i].removeAttribute('tabindex');
            }
        }        
    }
    //function for removing dropeffect from the target containers
    function clearDropeffects() {
        //if we have any selected items
        if(selections.items.length) {
            //reset aria-dropeffect and remove tabindex from all targets
            for(var len = targets.length, i = 0; i < len; i ++) {
                if(targets[i].getAttribute('aria-dropeffect') != 'none') {
                    targets[i].setAttribute('aria-dropeffect', 'none');
                    targets[i].removeAttribute('tabindex');
                }
            }
            //restore aria-grabbed and tabindex to all selectable items 
            //without changing the grabbed value of any existing selected items
            for(var len = items.length, i = 0; i < len; i ++) {
                if(!items[i].getAttribute('aria-grabbed')) {
                    items[i].setAttribute('aria-grabbed', 'false');
                    items[i].setAttribute('tabindex', '0');
                }
                else if(items[i].getAttribute('aria-grabbed') == 'true') {items[i].setAttribute('tabindex', '0');}
            }        
        }
    }
    //shortcut function for identifying an event element's target container
    function getContainer(element) {
        do{
            if(element.nodeType == 1 && element.getAttribute('aria-dropeffect')) {
                return element;
            }
        }
        while(element = element.parentNode);
        return null;
    }
    //mousedown event to implement single selection
    document.addEventListener('mousedown', function(e) {
        //if the element is a draggable item
        if(e.target.getAttribute('draggable')){
            //clear dropeffect from the target containers
            clearDropeffects();
            //if the multiple selection modifier is not pressed 
            //and the item's grabbed state is currently false
            if(!hasModifier(e) && e.target.getAttribute('aria-grabbed') == 'false') {
                //clear all existing selections
                clearSelections();
                //then add this new selection
                addSelection(e.target);
            }
        }
        //else [if the element is anything else]
        //and the selection modifier is not pressed 
        else if(!hasModifier(e)){
            //clear dropeffect from the target containers
            clearDropeffects();
            //clear all existing selections
            clearSelections();
        }
        //else [if the element is anything else and the modifier is pressed]
        else{
            //clear dropeffect from the target containers
            clearDropeffects();
        }
    }, false);
    //mouseup event to implement multiple selection
    document.addEventListener('mouseup', function(e) {
        //if the element is a draggable item 
        //and the multipler selection modifier is pressed
        if(e.target.getAttribute('draggable') && hasModifier(e)) {
            //if the item's grabbed state is currently true
            if(e.target.getAttribute('aria-grabbed') == 'true') {
                //unselect this item
                removeSelection(e.target);
                //if that was the only selected item
                //then reset the owner container reference
                if(!selections.items.length){
                    selections.owner = null;
                }
            }
            //else [if the item's grabbed state is false]
            else{addSelection(e.target);} //add this additional selection
        }
    }, false);
    //dragstart event to initiate mouse dragging
    document.addEventListener('dragstart', function(e) {
        //if the element's parent is not the owner, then block this event
        if(selections.owner != e.target.parentNode) {
            e.preventDefault();
            return;
        }   
        //[else] if the multiple selection modifier is pressed 
        //and the item's grabbed state is currently false
        if (hasModifier(e) && e.target.getAttribute('aria-grabbed') == 'false') {addSelection(e.target);}
        //we don't need the transfer data, but we have to define something
        //otherwise the drop action won't work at all in firefox
        //most browsers support the proper mime-type syntax, eg. "text/plain"
        //but we have to use this incorrect syntax for the benefit of IE10+
        e.dataTransfer.setData('text', '');
        //apply dropeffect to the target containers
        addDropeffects();
    }, false);
    //related variable is needed to maintain a reference to the 
    //dragleave's relatedTarget, since it doesn't have e.relatedTarget
    var related = null;
    //dragenter event to set that variable
    document.addEventListener('dragenter', function(e) {related = e.target;}, false);
    //dragleave event to maintain target highlighting using that variable
    document.addEventListener('dragleave', function(e) {
        //get a drop target reference from the relatedTarget
        var droptarget = getContainer(related);
        //if the target is the owner then it's not a valid drop target
        if(droptarget == selections.owner) {droptarget = null;}
        //if the drop target is different from the last stored reference
        //(or we have one of those references but not the other one)
        if(droptarget != selections.droptarget) {
            //if we have a saved reference, clear its existing dragover class
            if(selections.droptarget) {selections.droptarget.className = selections.droptarget.className.replace(/ dragover/g, '');}
            //apply the dragover class to the new drop target reference
            if(droptarget) {droptarget.className += ' dragover';}      
            //then save that reference for next time
            selections.droptarget = droptarget;
        }
    }, false);    
    //dragover event to allow the drag by preventing its default
    document.addEventListener('dragover', function(e) {
        //if we have any selected items, allow them to be dragged
        if(selections.items.length){
            e.preventDefault();
        }
    }, false);    
    //dragend event to implement items being validly dropped into targets,
    //or invalidly dropped elsewhere, and to clean-up the interface either way
    document.addEventListener('dragend', function(e)
    {
        //if we have a valid drop target reference
        //(which implies that we have some selected items)
        if(selections.droptarget) {
            //append the selected items to the end of the target container
            for(var len = selections.items.length, i = 0; i < len; i ++){selections.droptarget.appendChild(selections.items[i]);}
            //prevent default to allow the action            
            e.preventDefault();
        }
        //if we have any selected items
        if(selections.items.length) {
            //clear dropeffect from the target containers
            clearDropeffects();
            //if we have a valid drop target reference
            if(selections.droptarget) {
                //reset the selections array
                clearSelections();
                //reset the target's dragover class
                selections.droptarget.className = selections.droptarget.className.replace(/ dragover/g, '');
                //reset the target reference
                selections.droptarget = null;
            }
        }
    }, false);
}
/**
 * function which receive a CSV file by an HTML input file and parse it in an array of objects
 */
// ------------------------------------------------ CSV READING AND OUTPUT : ARRAY OF OBJECT
function handleDialog(event) {
    var files = event.target.files;
    var file = files[0];
    var reader = new FileReader();
    reader.readAsText(file);
    reader.onload = function(event){
        var csv = event.target.result;
        var data = $.csv.toObjects(csv);
        groupCall=data; // Initialize the static variable of the CSV file (array_format)
        return groupCall // It's a static variables, so doesn't need to return and can be used wherever you want on the script
    }
}
// --------------------------------------------- DRAW AND GROUP CREATION
    // Take the starter ARRAY --> SHUFFLE IT --> Ready for DIVIDE
    function drawRandom(anArray) {
        // Create a condition which check if the 'group'+i or 'reserve' property are defined in the objects of the csv file 
        // IF exist : Then return the 'anArray' parameter such as ;
        // ELSE execut the code below
        if(anArray[0].hasOwnProperty('Groupe')) {return anArray;}
        else{
            var alreadyDraw=[];
            while(alreadyDraw.length<anArray.length) {
                var minRand=0;
                var maxRand=anArray.length;
                var rand=Math.floor(Math.random()*(maxRand-minRand)+minRand);
                if(!valueInArray(anArray[rand],alreadyDraw)) {alreadyDraw.push(anArray[rand]);}
            }
            return alreadyDraw;
        }
    }
    // CHECK IF A VALUE IS IN AN ARRAY
    function valueInArray(value,array) {
        for(var i=0;i<array.length;i++) {
            if(array[i] == value){return true;}
        }
        return false;
    }
    // ARRAY to DIVIDE --> Slice 0, n ; P times --> P arrays + Array 'Reste' ;
    function createGroup(array,size=3,number=4) {
        var group=[];
        for(var i=0;i<number;i++){
            group[i]=[];
            for(var a=0;a<size;a++){
                group[i][a]=array[0];
                array.splice(0,1);
            }
        }
        group[i]=array;
        return group;
    }
    // ANOTHER VERSION, THAT MAKE ONLY FULL GROUP DEPEND ON SIZE PARAMETER
    function createGroupFull(array,size=3,number=4){
        if(array[0].hasOwnProperty('Groupe')){ // TEST IF CSV FILE ARRAY, have already been treated by our program
            var len=array.length;
            var indice=0;
            var number=0;
            var result=[];
            result[indice]=[];
            for(var b=0;b<len;b++){
                if(array[b].Groupe == 'group'+indice) {
                    result[indice][number]=array[b];
                    number++;
                }else if(array[b].Groupe != 'reserve') {
                    number=0;
                    result[++indice]=[];
                    b--;
                }
            }
            result[++indice]=[];
            number=0;
            for(var c=0;c<len;c++) {
                if(array[c].Groupe == "reserve") {
                    result[indice][number]=array[c];
                    number++;
                }
            }
            alert('Votre fichier à bien été reconnue !\nAucune modification supplémentaire n\'a été apporté.');
            return result;
        }else{
            var group=[];
            var delta=array.length%size;
            var nbrGrp;
            if(delta == 0) {nbrGrp=array.length/size;}
            else {nbrGrp=(array.length-delta)/size;}
            for(var i=0;i<number;i++) {
                group[i]=[];
                if(i<nbrGrp){
                    for(var a=0;a<size;a++) {
                        group[i][a]=array[0];
                        array.splice(0,1);
                    }
                }
            }
            group[i]=array;
            return group;
        }
    }
    // MAKE AN ARRAY OF REFERENT OF EACH GROUP RANDOMLY
    function chooseReferent(array) {
        var indexExclude=array.length-1;
        var referent=[];
        for(var i=0;i<indexExclude;i++ ){
            var rand=Math.floor(Math.random()*(array[i].length));
            referent[i]=array[i][rand];
        }
        return referent;
    }
    // FUNCTION MAIN, EXEC ALL OTHERS FUNCTION IN ONE
    function drawAndGroup(array,number=4,size=3) {
        var result=[];
        result[0]=drawRandom(array);
        result[0]=createGroupFull(result[0],size,number); // VERSION WITH ONLY FULL GROUP
        //result[1]=chooseReferent(result[0]); // OPTIONNAL, YOU CAN DELETE THIS LINE AND RETURN RESULT[0];
        return result[0];
    }
/** -- SIDE NOTES */
// Options : Selon taille tableau, plusieurs comportement possible 
// P : Nombre de tableaux à l'arrivé ; N : length des P;
// 1 Tab.length % P == 0 
// 2 Tab.length % P != 0 ; Floor(Tab.length / p) --> Dans P tableaux ; Tab.length % p --> Dans Tableau 'reste'
// 3 Tab.length % N != 0 ; Floor(tab.length / n) --> P tableaux ; Tab.length % n --> Tableau 'reste' || Dans p+1 Tableau

/**
 * Start getting value from li to create an array of objects
 */
//Get values from li on ol group and put it into an array of object
function getValueFromGroupLi () {   
    let olGroupList = $(".append ol");
    let arrGroupLi = [];
    for ( i = 0; i < olGroupList.length; i++) {
        let liOnEachOl = $(".group"+i+" li");
        let ol = document.getElementsByClassName("group"+i)[0];
        arrGroupLi[i] = [];
        for ( j = 0; j < liOnEachOl.length; j++) {
            arrGroupLi[i][j] = JSON.parse(ol.getElementsByTagName("li")[j].getAttribute("data-object"));
            arrGroupLi[i][j]["Groupe"] = "group"+i;
        }
    }
    return arrGroupLi;
}
//Get values from li on ol reserve and put it into an array of object
function loopInReserve() {
    let liReserveList = $(".reserve li");
    let arrReserveLi = [];
    let ol = document.getElementsByClassName("reserve")[0];
    for ( i = 0; i < liReserveList.length; i++) {
        arrReserveLi[i] = JSON.parse(ol.getElementsByTagName("li")[i].getAttribute("data-object"));
        arrReserveLi[i]["Groupe"] = "reserve";
    }
    return arrReserveLi;
}
//Function wich take arrGroupli and transform it in an array of simple objects to be used in the export funnction
function arrGroupLiToArrOfObjects (arr) {
    let arrGroupObject = [];
    for (let i=0; i <arr.length; i++){
        let arrlen = arr[i].length;
        for (let j=0; j <arrlen; j++){arrGroupObject.push(arr[i][j]);};
    };
    return arrGroupObject;
}
//Function of functions to get li values to array of array of objects
function getObject () {
    arrGroupLi = getValueFromGroupLi();
    arrReserveLi = loopInReserve();
    arrGroupLi.push(arrReserveLi);
    arrFinalForCsv = arrGroupLiToArrOfObjects(arrGroupLi);
    csv = $.csv.fromObjects(arrFinalForCsv);
    return csv
}
//function download csv file
function downloadCSV(args) {  
    var data, filename, link;
    var csv = getObject();
    if (csv == null) return;
    filename = args.filename || 'export.csv';
    var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) {navigator.msSaveBlob(blob, exportedFilenmae);}   // IE 10+
    else {
        var link = document.createElement("a");
        if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", filename);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
}
//Function of functions to generate reserve and groups
function makeGroupsAndReserve () {   
        // Input value & Filter it's value to make it an INT (by default, string);
        var nbrGrp=parseInt(document.getElementById("nbrGrp").value);
        var sizeGrp=parseInt(document.getElementById('sizeGrp').value);
        if(nbrGrp && sizeGrp){ // Value exist for BOTH, then call drawAndGroup() with all of it's parameters
            let group = drawAndGroup(groupCall,nbrGrp,sizeGrp);
            generateReserve(group);
            generateLiInOl(group);
            flag = true;
        }else{ // Call the function, only with the csv file (cause function have others parameters define by default 4 grp && size of 3)
            let group = drawAndGroup(groupCall)
            generateReserve(group);
            generateLiInOl(group);
            flag = true;
        }
        dragAndDrop();
}
// Function to prevent user to launch before insert a file
/** Version with a lot of unexpected behavior
 * Maybe will be updated later !
function testBeforeLaunch(){
    // Test if files is insered before the call of function 
    if(typeof groupCall == 'undefined' || groupCall == 'null'){
        return alert('Aucun fichier insérée ! ');
    }else{
    // Initialize variables for the test
        var toTest=drawAndGroup(groupCall); // Array to test to check if the object have property
        var propCount=0;
        console.log(toTest);
        // Test if the object of the array who will used later, have more than 1 properties ;
        // If no prop is find, means that the csv reader was unable to create some (cause of wrong type of file); DOESN'T WORK !

        // New solution --> Test if the Object[prop] == undefined
        for(var i=0;i<toTest.length;i++){
            for(var a=0;a<toTest[i].length;a++){
                var obj=toTest[i][a];
                for(var propKey in obj){
                    var returnOfTest=obj[propKey];
                    console.log('prop_name :'+propKey);
                    console.log(returnOfTest);
                }
            }
        }
        if(returnOfTest==true){
            return alert('Format de fichier non valide !\nUn fichier csv est requis AVEC au moins 2 colonnes !');
        }else{
            makeGroupsAndReserve();
            $(".output").toggle();
            disable();
        }
    }
}
*/
function testBeforeLaunch(){
    var filename=document.getElementById('files').value;
    var fileExt=filename.split('.').pop();
    if(typeof groupCall == 'undefined' || groupCall == 'null'){return alert('Aucun fichier insérée ! ');}
    else{
        if(fileExt!='csv'){return alert('Extension de fichier non pris en charge !\nFichier CSV uniquement.');}
        else{
            makeGroupsAndReserve();
            $(".output").toggle();
            disable();
        }
    }
}
/**
 * Function to use on index with jQuery function call
 */
//Function to show arrows button
function showArrows() {
    $(this).children("button").css("display", "inline-block");
}
//Function to hide arrows
function hideArrows() {
    $(this).children("button").css("display", "none");
}
/**
 * Functions to moveLi in group
 */
//Move li up
function moveLiDown() {
    var presentLi = $(this).closest("li");
    presentLi.closest("ol").children(":eq(" + (presentLi.index() + 1) + ")").after(presentLi);
}
//Move li down
function moveLiUp() {
    var presentLi = $(this).closest("li");
    let index = presentLi.index();
    if (index != 0) presentLi.closest("ol").children(":eq(" + (presentLi.index() - 1) + ")").before(presentLi);
}
/**
 * Function to disable click me button
 */
function disable() {
    $("#clickme").attr("disabled", true);
}